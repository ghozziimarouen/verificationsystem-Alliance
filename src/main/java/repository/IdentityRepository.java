/**
 * 
 */
package repository;

import org.springframework.data.repository.CrudRepository;

import entities.Identity;

/**
 * @author mghozzi
 *
 * @AlienCompany copyWright
 */
public interface IdentityRepository extends CrudRepository<Identity, Integer> {

	
	

}
