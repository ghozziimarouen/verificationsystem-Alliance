/**
 * 
 */
package entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author mghozzi
 *
 * @AlienCompany copyWright
 */
@Entity
@Table(name = "identity")
public class Identity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8839378070099357292L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private String id;
	@Column
	private String identityNumber;

	public Identity(String id, String identityNumber) {
		super();
		this.id = id;
		this.identityNumber = identityNumber;
	}

	public String getIdentityNumber() {
		return identityNumber;
	}

	public void setIdentityNumber(String identityNumber) {
		this.identityNumber = identityNumber;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
