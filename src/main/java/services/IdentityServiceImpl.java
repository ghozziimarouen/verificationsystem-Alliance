/**
 * 
 */
package services;

import org.springframework.beans.factory.annotation.Autowired;

import entities.Identity;
import repository.IdentityRepository;

/**
 * @author mghozzi
 *
 * @AlienCompany copyWright
 */

public class IdentityServiceImpl implements IdentityService {
	@Autowired
	IdentityRepository identityRepository;

	@Override
	public Identity addUser(Identity u) {
		return identityRepository.save(u);
	}

}
