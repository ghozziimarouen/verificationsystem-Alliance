/**
 * 
 */
package services;

import entities.Identity;

/**
 * @author mghozzi
 *
 * @AlienCompany copyWright
 */
public interface IdentityService {
	Identity addUser(Identity u);
}
